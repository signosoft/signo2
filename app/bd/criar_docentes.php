  <?php
    require_once __DIR__ . '/../modelos/exportar.php';
    require_once __DIR__ . '/../repositorios/exportar.php';
    require_once __DIR__ . '/../controladores/exportar.php';

    $controlador = new UtilizadorControlador;

    $repositorio = new UtilizadorRepositorio;
    $utilizadores = array(
      array('id' => null, 'email' => 'rubenmanhica@gmail.com', 'utilizador' => 'rubenmanhica','salt' => null, 'password' => 'rubenmanhica', 'desactivado' => false),
      array('id' => null, 'email' => 'valiissufo@gmail.com',  'utilizador' => 'valiissufo','salt' => null, 'password' => 'valiissufo', 'desactivado' => false),
      array('id' => null, 'email' => 'tatianakovalenko@gmail.com',  'utilizador' => 'tatianakovalenko','salt' => null, 'password' => 'tatianakovalenko', 'desactivado' => false),
      array('id' => null, 'email' => 'lourinochemane@gmail.com',  'utilizador' => 'lourinochemane','salt' => null, 'password' => 'lourinochemane', 'desactivado' => false),
      array('id' => null, 'email' => 'assanecipriano@hotmail.com',  'utilizador' => 'assanecipriano','salt' => null, 'password' => 'assanecipriano', 'desactivado' => false),
      array('id' => null, 'email' => 'benildojoaquim@gmail.com',  'utilizador' => 'benildojoaquim','salt' => null, 'password' => 'benildojoaquim', 'desactivado' => false),
      array('id' => null, 'email' => 'leilaomar@gmail.com',  'utilizador' => 'leilaomar','salt' => null, 'password' => 'leilaomar', 'desactivado' => false),
      array('id' => null, 'email' => 'albinocuinhane@gmail.com',  'utilizador' => 'albinocuinhaque','salt' => null, 'password' => 'albinocuinhane', 'desactivado' => false)
    );

    $docentes = array(
      array('id' => null, 'nome' => 'Ruben Manhica', 'utilizador' => null, 'email' => 'rubenmanhica@gmail.com', 'utilizador' => 'rubenmanhica', 'senha' => 'albinocuinhane'),
      array('id' => null, 'nome' => 'Vali Issufo', 'utilizador' => null, 'email' => 'valiissufo@gmail.com', 'utilizador' => 'valiissufo','senha' => 'albinocuinhane'),
      array('id' => null, 'nome' => 'Tatiana kovalenko', 'utilizador' => null, 'email' => 'tatianakovalenko@gmail.com', 'utilizador' => 'tatianakovalenko','senha' => 'albinocuinhane'),
      array('id' => null, 'nome' => 'Lourino Chemane', 'utilizador' => null, 'email' => 'lourinochemane@gmail.com', 'utilizador' => 'lourinochemane','senha' => 'albinocuinhane'),
      array('id' => null, 'nome' => 'Assane Cipriano', 'utilizador' => null, 'email' => 'assanecipriano@gmail.com', 'utilizador' => 'assanecipriano','senha' => 'albinocuinhane'),
      array('id' => null, 'nome' => 'Benildo Joaquim', 'utilizador' => null, 'email' => 'benildojoaquim@gmail.com', 'utilizador' => 'benildojoaquim','senha' => 'albinocuinhane'),
      array('id' => null, 'nome' => 'Leila Omar', 'utilizador' => null, 'email' => 'leilaomar@gmail.com', 'utilizador' => 'leilaomar','senha' => 'albinocuinhane'),
      array('id' => null, 'nome' => 'Albino Cuinhane', 'utilizador' => null, 'email' => 'albinocuinhane@gmail.com', 'utilizador' => 'albino cuinhane','senha' => 'albinocuinhane')
    );

    foreach ($utilizadores as $u) {
      $senha = new Senha;
      $utilizador = new Utilizador($u['id'], $u['email'], $u['utilizador'], $senha->salt(), $senha->password($u['password']), $u['desactivado']);
      if($repositorio->criar($utilizador)) {
        echo 'O utilizador ' . $utilizador->email() . ' foi criado com sucesso' . "\n";
      } else {
        echo 'O utilizador ' . $utilizador->email() . ' ja existe' . "\n";
      }
    }

    foreach ($docentes as $u) {
      $utilizador = $controlador->encontrar($u['email']);
      print_r($utilizador);
      if ($utilizador) {
        $docente = new Docente($u['id'], $u['nome'], $utilizador);
        $docenteControlador = new DocenteControlador;
        if ($docenteControlador->criar($u['nome'], $utilizador)) {
          echo 'O docente ' . $docente->nome() . ' foi criado com sucesso' . "\n";
        } else {
          echo 'O docente ' . $docente->nome() . ' ja existe' . "\n";
        }
      }
    }

  ?>
