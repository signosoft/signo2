<?php
    require_once __DIR__ . '/../modelos/exportar.php';
    require_once __DIR__ . '/../repositorios/exportar.php';
    require_once __DIR__ . '/../controladores/exportar.php';

    $repositorio = new UtilizadorRepositorio;
    $controlador = new UtilizadorControlador;

    $utilizadores = array(
      array('id' => null, 'email' => 'edsonmichaque@yandex.com', 'salt' => null, 'password' => 'edsonmichaque', 'desactivado' => false),
      array('id' => null, 'email' => 'densquejamal@gmail.com', 'salt' => null, 'password' => 'densquejamal', 'desactivado' => false),
      array('id' => null, 'email' => 'albertocremildomoiane@gmail.com', 'salt' => null, 'password' => 'albertocremildomoiane', 'desactivado' => false),
      array('id' => null, 'email' => 'valtercheque@gmail.com', 'salt' => null, 'password' => 'valtercheque', 'desactivado' => false),
      array('id' => null, 'email' => 'eltonlaice@hotmail.com', 'salt' => null, 'password' => 'eltonlaice', 'desactivado' => false),
      array('id' => null, 'email' => 'celesteorlando12@gmail.com', 'salt' => null, 'password' => 'celesteorlando12', 'desactivado' => false),
      array('id' => null, 'email' => 'muzimeunice95@gmail.com', 'salt' => null, 'password' => 'muzimeunice95', 'desactivado' => false),
      array('id' => null, 'email' => 'uem.assane@gmail.com', 'salt' => null, 'password' => 'uem.assane', 'desactivado' => false),
      array('id' => null, 'email' => 'vanydina.coutinho@gmail.com', 'salt' => null, 'password' => 'vanydina.coutinho', 'desactivado' => false),
      array('id' => null, 'email' => 'julianelmabo@gmail.com', 'salt' => null, 'password' => 'julianelmab', 'desactivado' => false),
      array('id' => null, 'email' => 'paulozinessa@gmail.com', 'salt' => null, 'password' => 'paulozinessa', 'desactivado' => false),
      array('id' => null, 'email' => 'tedyivan255@gmail.com', 'salt' => null, 'password' => 'tedyivan255', 'desactivado' => false),
      array('id' => null, 'email' => 'edsonbeats@gmail.com', 'salt' => null, 'password' => 'edsonbeats', 'desactivado' => false),
    );

    foreach ($utilizadores as $u) {
      $utilizador = new Utilizador($u['id'], $u['email'], null, $u['password'], $u['desactivado']);
      if($controlador->autenticar($utilizador)) {
        echo 'O utilizador ' . $utilizador->email() . ' foi autenticado com sucesso' . "\n";
      } else {
        echo 'O utilizador ' . $utilizador->email() . ' nao foi autenticado' . "\n";
      }
    }
?>
