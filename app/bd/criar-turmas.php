<?php
    require_once __DIR__ . '/../modelos/exportar.php';
    require_once __DIR__ . '/../repositorios/exportar.php';
    require_once __DIR__ . '/../controladores/exportar.php';

    $repositorio = new RepositorioGenerico;
    $repositorioDeTurmas = new TurmaRepositorio;
    $tr = new TurmaRepositorio;

    $repositorio->removerTodos('niveis');
    $repositorio->removerTodos('cursos');
    $repositorio->removerTodos('turnos');
    $repositorio->removerTodos('disciplinas');
    $repositorioDeTurmas->removerTodas();

    $niveis = array(
      array('id' => null, 'nome' => 'Nível 1', 'codigo' => 'NIVEL1'),
      array('id' => null, 'nome' => 'Nível 2', 'codigo' => 'NIVEL2'),
      array('id' => null, 'nome' => 'Nível 3', 'codigo' => 'NIVEL3'),
      array('id' => null, 'nome' => 'Nível 4', 'codigo' => 'NIVEL4'),
      array('id' => null, 'nome' => 'Nível 5', 'codigo' => 'NIVEL5'),
    );

    $cursos = array(
      array('id' => null, 'nome' => 'Informática', 'codigo' => 'INFORMATICA'),
      array('id' => null, 'nome' => 'Electrónica', 'codigo' => 'ELECTRONICA'),
      array('id' => null, 'nome' => 'Eléctrica', 'codigo' => 'ELECTRICA')
    );

    $turnos = array(
      array('id' => null, 'nome' => 'Diurno', 'codigo' => 'DIURNO'),
      array('id' => null, 'nome' => 'Nocturno', 'codigo' => 'NOCTURNO')
    );


    foreach ($niveis as $n) {
      $nivel = new Nivel($n['id'], $n['nome'], $n['codigo']);

      if($repositorio->criar($nivel)) {
        echo 'O Nivel ' . $nivel->getNome() . ' foi criado com sucesso' . "\n";
      } else {
        echo 'O Nivel ' . $nivel->getNome() . ' ja existe' . "\n";
      }
    }

    foreach ($cursos as $n) {
      $curso = new Curso($n['id'], $n['nome'], $n['codigo']);

      if($repositorio->criar($curso)) {
        echo 'O Curso ' . $curso->getNome() . ' foi criado com sucesso' . "\n";
      } else {
        echo 'O Curso ' . $curso->getNome() . ' ja existe' . "\n";
      }
    }

    foreach ($turnos as $n) {
      $turno = new Turno($n['id'], $n['nome'], $n['codigo']);

      if($repositorio->criar($turno)) {
        echo 'O Turno ' . $turno->getNome() . ' foi criado com sucesso' . "\n";
      } else {
        echo 'O Turno ' . $turno->getNome() . ' ja existe' . "\n";
      }
    }

    $listaDeCursos = $repositorio->todos('cursos');
    $listaDeNiveis = $repositorio->todos('niveis');
    $listaDeTurnos = $repositorio->todos('turnos');
    $listaDeDisciplinas = $repositorio->todos('disciplinas');

    foreach($listaDeTurnos as $turno) {
      foreach($listaDeCursos as $curso) {
        foreach($listaDeNiveis as $nivel) {
          $repositorioDeTurmas->criar(new Turma(null, $curso, $nivel, $turno));
        }
      }
    }

    $disciplinas = array();

    $disciplinas['SEM1'] = array(
      array('nome' => 'AMI', 'codigo' => 'AMI'),
      array('nome' => 'ALGA', 'codigo' => 'ALGA'),
      array('nome' => 'MDI', 'codigo' => 'MDI'),
      array('nome' => 'Informática', 'codigo' => 'INFORMATICA'),
      array('nome' => 'IaE', 'codigo' => 'IaE'),
      array('nome' => 'Fisica', 'codigo' => 'FISICA'),
    );

    $disciplinas['SEM2'] = array(
      array('nome' => 'AMII', 'codigo' => 'AMII'),
      array('nome' => 'AdC', 'codigo' => 'AdC'),
      array('nome' => 'MDII', 'codigo' => 'MDII'),
      array('nome' => 'IaP', 'codigo' => 'IaP'),
      array('nome' => 'DAC', 'codigo' => 'DAC'),
      array('nome' => 'IeM', 'codigo' => 'IEM'),
    );

    $disciplinas['SEM3'] = array(
      array('nome' => 'AMIII', 'codigo' => 'AMIII'),
      array('nome' => 'EA', 'codigo' => 'EA'),
      array('nome' => 'BDI', 'codigo' => 'BDI'),
      array('nome' => 'POOI', 'codigo' => 'POOI'),
      array('nome' => 'PME', 'codigo' => 'PME'),
      array('nome' => 'LP', 'codigo' => 'LP'),
    );

    $disciplinas['SEM4'] = array(
      array('nome' => 'MN', 'codigo' => 'MN'),
      array('nome' => 'POO II', 'codigo' => 'POO2'),
      array('nome' => 'BDII', 'codigo' => 'BDII'),
      array('nome' => 'ED', 'codigo' => 'ED'),
      array('nome' => 'SM', 'codigo' => 'SM'),
      array('nome' => 'EDA', 'codigo' => 'EDA'),
    );

    $disciplinas['SEM5'] = array(
      array('nome' => 'ES I', 'codigo' => 'ES1'),
      array('nome' => 'Redes I', 'codigo' => 'REDES1'),
      array('nome' => 'MP', 'codigo' => 'MP'),
      array('nome' => 'Ingles I', 'codigo' => 'INGLES1'),
      array('nome' => 'SOPC', 'codigo' => 'SOPC'),
      array('nome' => 'PWSGC', 'codigo' => 'PWSGC'),
    );

    $disciplinas['SEM6'] = array(
      array('nome' => 'GE', 'codigo' => 'ES'),
      array('nome' => 'HC', 'codigo' => 'HC'),
      array('nome' => 'Redes II', 'codigo' => 'REDES2'),
      array('nome' => 'ES II', 'codigo' => 'ES2'),
      array('nome' => 'IA I', 'codigo' => 'IA1'),
      array('nome' => 'IO', 'codigo' => 'IO'),
    );

    $disciplinas['SEM7'] = array(
      array('nome' => 'IA II', 'codigo' => 'IA2'),
      array('nome' => 'CeSD', 'codigo' => 'CESD'),
      array('nome' => 'AeSSC', 'codigo' => 'AESSC'),
      array('nome' => 'Compiladores', 'codigo' => 'COMPILADORES'),
      array('nome' => 'OdI', 'codigo' => 'ODI')
    );


    $disciplinas['SEM8'] = array(
      array('nome' => 'SSeA', 'codigo' => 'SSEA'),
      array('nome' => 'AEP', 'codigo' => 'AEP'),
      array('nome' => 'CG', 'codigo' => 'CG'),
      array('nome' => 'SD', 'codigo' => 'SD'),
      array('nome' => 'PIA', 'codigo' => 'PIA')
    );

    $disciplinas['SEM9'] = array(
      array('nome' => 'EP', 'codigo' => 'EP'),
      array('nome' => 'TL', 'codigo' => 'TL')
    );

    foreach($disciplinas as $semestre) {
      foreach($semestre as $d) {
        $disc = new Disciplina(null, $d['nome'], $d['codigo']);
        if($repositorio->criar($disc, 'disciplinas')) {
          echo 'A Disciplina ' . $disc->getNome() . ' foi criado com sucesso' . "\n";
        } else {
          echo 'A Disciplina ' . $disc->getNome() . ' ja existe' . "\n";
        }
      }
    }

    /*
    $listaDeTurmas = $repositorioDeTurmas->todasTurmas();
    $TurmasDeInformatica = $repositorioDeTurmas->todasTurmasPorCurso($listaDeCursos[0]);
    $TurmasDeElectronica = $repositorioDeTurmas->todasTurmasPorCurso($listaDeCursos[1]);
    $TurmasDeElectrica = $repositorioDeTurmas->todasTurmasPorCurso($listaDeCursos[2]);

    $TurmasDeInformaticaNivel1 = $repositorioDeTurmas->todasTurmasPorCusroNivelTurno($listaDeCursos[0]->getId(), $listaDeNiveis[1]->getId(), $listaDeTurnos[0]->getId());
    */

    $i1 = $tr->turmaDoInformaticaNivel1Diurno();
    $i2 = $tr->turmaDoInformaticaNivel2Diurno();
    $i3 = $tr->turmaDoInformaticaNivel3Diurno();
    $i4 = $tr->turmaDoInformaticaNivel4Diurno();
    $i5 = $tr->turmaDoInformaticaNivel5Diurno();

    foreach($disciplinas['SEM1'] as $disc) {
      echo $disc['codigo'] . "\n";
      $d = $repositorio->encontrarPorCodigo($disc['codigo'], 'disciplinas');
      $repositorio->ligarDisciplinaATurma($d, $i1);
    }

    foreach($disciplinas['SEM2'] as $disc) {
      echo $disc['codigo'] . "\n";
      $d = $repositorio->encontrarPorCodigo($disc['codigo'], 'disciplinas');
      $repositorio->ligarDisciplinaATurma($d, $i1);
    }

    foreach($disciplinas['SEM3'] as $disc) {
      echo $disc['codigo'] . "\n";
      $d = $repositorio->encontrarPorCodigo($disc['codigo'], 'disciplinas');
      $repositorio->ligarDisciplinaATurma($d, $i2);
    }

    foreach($disciplinas['SEM4'] as $disc) {
      echo $disc['codigo'] . "\n";
      $d = $repositorio->encontrarPorCodigo($disc['codigo'], 'disciplinas');
      $repositorio->ligarDisciplinaATurma($d, $i2);
    }

    foreach($disciplinas['SEM5'] as $disc) {
      echo $disc['codigo'] . "\n";
      $d = $repositorio->encontrarPorCodigo($disc['codigo'], 'disciplinas');
      $repositorio->ligarDisciplinaATurma($d, $i3);
    }

    foreach($disciplinas['SEM6'] as $disc) {
      echo $disc['codigo'] . "\n";
      $d = $repositorio->encontrarPorCodigo($disc['codigo'], 'disciplinas');
      $repositorio->ligarDisciplinaATurma($d, $i3);
    }

    foreach($disciplinas['SEM7'] as $disc) {
      echo $disc['codigo'] . "\n";
      $d = $repositorio->encontrarPorCodigo($disc['codigo'], 'disciplinas');
      $repositorio->ligarDisciplinaATurma($d, $i4);
    }

    foreach($disciplinas['SEM8'] as $disc) {
      echo $disc['codigo'] . "\n";
      $d = $repositorio->encontrarPorCodigo($disc['codigo'], 'disciplinas');
      $repositorio->ligarDisciplinaATurma($d, $i4);
    }

    foreach($disciplinas['SEM9'] as $disc) {
      echo $disc['codigo'] . "\n";
      $d = $repositorio->encontrarPorCodigo($disc['codigo'], 'disciplinas');
      $repositorio->ligarDisciplinaATurma($d, $i5);
    }

  ?>
