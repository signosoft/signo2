<?php
require_once __DIR__.'/../modelos/exportar.php';
require_once __DIR__.'/../repositorios/exportar.php';

$control = new CategoriaDeAvaliacaoRepositorio;

$categorias = array(
	array('id' => null, 'descricao' => 'Teste'),
	array('id' => null, 'descricao' => 'mini-Teste'),
	array('id' => null, 'descricao' => 'Trabalho')
	);

foreach($categorias as $categ ){
	$ct = new CategoriaDeAvaliacao($categ['id'],$categ['descricao']);
	if($control->criar($ct)){
		echo "Categoria " . $categ['descricao'] . " criada com sucesso!" . "\n";
	} else{
		echo "Categoria " . $categ['descricao'] . " ja existe!" . "\n";
	}
}

?>