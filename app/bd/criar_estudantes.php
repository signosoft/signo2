<?php
    require_once __DIR__ . '/../modelos/exportar.php';
    require_once __DIR__ . '/../repositorios/exportar.php';
    require_once __DIR__ . '/../controladores/exportar.php';

    $repositorio = new UtilizadorRepositorio;
    $controlador = new EstudanteControlador;

    $estudantes = array(
      array('id' => null, 'email' => 'edsonmichaque@yandex.com', 'cartao' => '20130000', 'nome' => 'Edson Michaque'),
      array('id' => null, 'email' => 'densquejamal@gmail.com', 'cartao' => '20130001', 'nome' => 'Densque Jamal'),
      array('id' => null, 'email' => 'albertocremildomoiane@gmail.com', 'cartao' => '20130002', 'nome' => 'Alberto Moiane'),
      array('id' => null, 'email' => 'valtercheque@gmail.com', 'cartao' => '20130003', 'nome' => 'Valter Cheque'),
      array('id' => null, 'email' => 'eltonlaice@hotmail.com', 'cartao' => '20130004', 'nome' => 'Elton Laice'),
      array('id' => null, 'email' => 'celesteorlando12@gmail.com', 'cartao' => '20130005', 'nome' => 'Celeste Rungo'),
      array('id' => null, 'email' => 'muzimeunice95@gmail.com', 'cartao' => '20130006', 'nome' => 'Eunice Muzime'),
      array('id' => null, 'email' => 'uem.assane@gmail.com', 'cartao' => '20130007', 'nome' => 'Muarucha Assane'),
      array('id' => null, 'email' => 'vanydina.coutinho@gmail.com', 'cartao' => '20130008', 'nome' => 'Vania Coutinho'),
      array('id' => null, 'email' => 'julianelmabo@gmail.com', 'cartao' => '20130009', 'nome' => 'Julia Beula'),
      array('id' => null, 'email' => 'paulozinessa@gmail.com', 'cartao' => '20130010', 'nome' => 'Paulo Zinessa'),
      array('id' => null, 'email' => 'tedyivan255@gmail.com', 'cartao' => '20130011', 'nome' => 'Tedy Macie'),
      array('id' => null, 'email' => 'edsonbeats@gmail.com', 'cartao' => '20130012', 'nome' => 'Edson Magaure'),
    );

    foreach ($estudantes as $u) {
      $uc = new UtilizadorControlador;
      $utilizador = $uc->encontrar($u['email']);

      if ($utilizador) {
        $estudante = new Estudante($u['id'], $u['cartao'], $u['nome'], $utilizador);
        $estudanteControlador = new EstudanteControlador;
        if ($estudanteControlador->criar($u['cartao'], $u['nome'], $utilizador)) {
          echo 'O Estudante ' . $estudante->getNome() . ' foi criado com sucesso' . "\n";
        } else {
          echo 'O Estudante ' . $estudante->getNome() . ' ja existe' . "\n";
        }
      }
    }