-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: 03-Jun-2015 às 06:51
-- Versão do servidor: 5.5.36
-- PHP Version: 5.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `teste`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cursos`
--

CREATE TABLE IF NOT EXISTS `cursos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=197 ;

--
-- Extraindo dados da tabela `cursos`
--

INSERT INTO `cursos` (`id`, `nome`, `codigo`) VALUES
(194, 'Informática', 'INFORMATICA'),
(195, 'Electrónica', 'ELECTRONICA'),
(196, 'Eléctrica', 'ELECTRICA');

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplinas`
--

CREATE TABLE IF NOT EXISTS `disciplinas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) NOT NULL,
  `codigo` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1129 ;

--
-- Extraindo dados da tabela `disciplinas`
--

INSERT INTO `disciplinas` (`id`, `nome`, `codigo`) VALUES
(1081, 'AMI', 'AMI'),
(1082, 'ALGA', 'ALGA'),
(1083, 'MDI', 'MDI'),
(1084, 'Informática', 'INFORMATICA'),
(1085, 'IaE', 'IaE'),
(1086, 'Fisica', 'FISICA'),
(1087, 'AMII', 'AMII'),
(1088, 'AdC', 'AdC'),
(1089, 'MDII', 'MDII'),
(1090, 'IaP', 'IaP'),
(1091, 'DAC', 'DAC'),
(1092, 'IeM', 'IEM'),
(1093, 'AMIII', 'AMIII'),
(1094, 'EA', 'EA'),
(1095, 'BDI', 'BDI'),
(1096, 'POOI', 'POOI'),
(1097, 'PME', 'PME'),
(1098, 'LP', 'LP'),
(1099, 'MN', 'MN'),
(1100, 'POO II', 'POO2'),
(1101, 'BDII', 'BDII'),
(1102, 'ED', 'ED'),
(1103, 'SM', 'SM'),
(1104, 'EDA', 'EDA'),
(1105, 'ES I', 'ES1'),
(1106, 'Redes I', 'REDES1'),
(1107, 'MP', 'MP'),
(1108, 'Ingles I', 'INGLES1'),
(1109, 'SOPC', 'SOPC'),
(1110, 'PWSGC', 'PWSGC'),
(1111, 'GE', 'ES'),
(1112, 'HC', 'HC'),
(1113, 'Redes II', 'REDES2'),
(1114, 'ES II', 'ES2'),
(1115, 'IA I', 'IA1'),
(1116, 'IO', 'IO'),
(1117, 'IA II', 'IA2'),
(1118, 'CeSD', 'CESD'),
(1119, 'AeSSC', 'AESSC'),
(1120, 'Compiladores', 'COMPILADORES'),
(1121, 'OdI', 'ODI'),
(1122, 'SSeA', 'SSEA'),
(1123, 'AEP', 'AEP'),
(1124, 'CG', 'CG'),
(1125, 'SD', 'SD'),
(1126, 'PIA', 'PIA'),
(1127, 'EP', 'EP'),
(1128, 'TL', 'TL');

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplina_turma`
--

CREATE TABLE IF NOT EXISTS `disciplina_turma` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `disciplina_id` int(11) NOT NULL,
  `turma_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `disciplina_id` (`disciplina_id`,`turma_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Extraindo dados da tabela `disciplina_turma`
--

INSERT INTO `disciplina_turma` (`id`, `disciplina_id`, `turma_id`) VALUES
(1, 1081, 1472),
(2, 1082, 1472),
(3, 1083, 1472),
(4, 1084, 1472),
(5, 1085, 1472),
(6, 1086, 1472),
(7, 1087, 1472),
(8, 1088, 1472),
(9, 1089, 1472),
(10, 1090, 1472),
(11, 1091, 1472),
(12, 1092, 1472),
(13, 1093, 1473),
(14, 1094, 1473),
(15, 1095, 1473),
(16, 1096, 1473),
(17, 1097, 1473),
(18, 1098, 1473),
(19, 1099, 1473),
(20, 1100, 1473),
(21, 1101, 1473),
(22, 1102, 1473),
(23, 1103, 1473),
(24, 1104, 1473),
(25, 1105, 1474),
(26, 1106, 1474),
(27, 1107, 1474),
(28, 1108, 1474),
(29, 1109, 1474),
(30, 1110, 1474),
(31, 1111, 1474),
(32, 1112, 1474),
(33, 1113, 1474),
(34, 1114, 1474),
(35, 1115, 1474),
(36, 1116, 1474),
(37, 1117, 1475),
(38, 1118, 1475),
(39, 1119, 1475),
(40, 1120, 1475),
(41, 1121, 1475),
(42, 1122, 1475),
(43, 1123, 1475),
(44, 1124, 1475),
(45, 1125, 1475),
(46, 1126, 1475),
(47, 1127, 1476),
(48, 1128, 1476);

-- --------------------------------------------------------

--
-- Estrutura da tabela `docente`
--

CREATE TABLE IF NOT EXISTS `docente` (
  `id` int(128) NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) NOT NULL,
  `utilizador_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `utilizador_id` (`utilizador_id`),
  KEY `utilizador_id_2` (`utilizador_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=115 ;

--
-- Extraindo dados da tabela `docente`
--

INSERT INTO `docente` (`id`, `nome`, `utilizador_id`) VALUES
(108, 'Ruben Manhica', 197),
(109, 'Vali Issufo', 198),
(110, 'Tatiana kovalenko', 199),
(111, 'Lourino Chemane', 200),
(112, 'Benildo Joaquim', 202),
(113, 'Leila Omar', 203),
(114, 'Albino Cuinhane', 204);

-- --------------------------------------------------------

--
-- Estrutura da tabela `estudante`
--

CREATE TABLE IF NOT EXISTS `estudante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) NOT NULL,
  `numero_de_estudante` varchar(8) NOT NULL,
  `utilizador_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `utilizador_id` (`utilizador_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Extraindo dados da tabela `estudante`
--

INSERT INTO `estudante` (`id`, `nome`, `numero_de_estudante`, `utilizador_id`) VALUES
(14, 'Edson Michaque', '20130000', 184),
(15, 'Densque Jamal', '20130001', 185),
(16, 'Alberto Moiane', '20130002', 186),
(17, 'Valter Cheque', '20130003', 187),
(18, 'Elton Laice', '20130004', 188),
(19, 'Celeste Rungo', '20130005', 189),
(20, 'Eunice Muzime', '20130006', 190),
(21, 'Muarucha Assane', '20130007', 191),
(22, 'Vania Coutinho', '20130008', 192),
(23, 'Paulo Zinessa', '20130010', 194),
(24, 'Tedy Macie', '20130011', 195);

-- --------------------------------------------------------

--
-- Estrutura da tabela `niveis`
--

CREATE TABLE IF NOT EXISTS `niveis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=349 ;

--
-- Extraindo dados da tabela `niveis`
--

INSERT INTO `niveis` (`id`, `nome`, `codigo`) VALUES
(344, 'Nível 1', 'NIVEL1'),
(345, 'Nível 2', 'NIVEL2'),
(346, 'Nível 3', 'NIVEL3'),
(347, 'Nível 4', 'NIVEL4'),
(348, 'Nível 5', 'NIVEL5');

-- --------------------------------------------------------

--
-- Estrutura da tabela `plano_analitico`
--

CREATE TABLE IF NOT EXISTS `plano_analitico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `turma_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `turma_id` (`turma_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `turmas`
--

CREATE TABLE IF NOT EXISTS `turmas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `curso_id` int(11) NOT NULL,
  `nivel_id` int(11) NOT NULL,
  `turno_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `curso_id` (`curso_id`,`nivel_id`),
  KEY `nivel_id` (`nivel_id`),
  KEY `turno_id` (`turno_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1502 ;

--
-- Extraindo dados da tabela `turmas`
--

INSERT INTO `turmas` (`id`, `curso_id`, `nivel_id`, `turno_id`) VALUES
(1472, 194, 344, 129),
(1473, 194, 345, 129),
(1474, 194, 346, 129),
(1475, 194, 347, 129),
(1476, 194, 348, 129),
(1477, 195, 344, 129),
(1478, 195, 345, 129),
(1479, 195, 346, 129),
(1480, 195, 347, 129),
(1481, 195, 348, 129),
(1482, 196, 344, 129),
(1483, 196, 345, 129),
(1484, 196, 346, 129),
(1485, 196, 347, 129),
(1486, 196, 348, 129),
(1487, 194, 344, 130),
(1488, 194, 345, 130),
(1489, 194, 346, 130),
(1490, 194, 347, 130),
(1491, 194, 348, 130),
(1492, 195, 344, 130),
(1493, 195, 345, 130),
(1494, 195, 346, 130),
(1495, 195, 347, 130),
(1496, 195, 348, 130),
(1497, 196, 344, 130),
(1498, 196, 345, 130),
(1499, 196, 346, 130),
(1500, 196, 347, 130),
(1501, 196, 348, 130);

-- --------------------------------------------------------

--
-- Estrutura da tabela `turnos`
--

CREATE TABLE IF NOT EXISTS `turnos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=131 ;

--
-- Extraindo dados da tabela `turnos`
--

INSERT INTO `turnos` (`id`, `nome`, `codigo`) VALUES
(129, 'Diurno', 'DIURNO'),
(130, 'Nocturno', 'NOCTURNO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `utilizador`
--

CREATE TABLE IF NOT EXISTS `utilizador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `utilizador` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL,
  `desactivado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=205 ;

--
-- Extraindo dados da tabela `utilizador`
--

INSERT INTO `utilizador` (`id`, `email`, `utilizador`, `password`, `salt`, `desactivado`) VALUES
(184, 'edsonmichaque@yandex.com', 'edsonmichaque', '7803b03e0dc3fe839cdf9ba9b55d3c4d0f3a8e26', 'be071754fa49bf96e198f6e6c262e9094f1b6d26', 0),
(185, 'densquejamal@gmail.com', 'densquejamal', '7489d0ea8e7beeb1ffbd1442ebdee98e1d6fab13', '34fab4d12c494f6810aa71bb738fa5f8ce9b004b', 0),
(186, 'albertocremildomoiane@gmail.com', 'albertocremildomoiane', 'dd27b5499eab3e33a779dce2074ae2a6556d3e9c', '3cfe190728949f11063179e6feb91ac4f2978f9a', 0),
(187, 'valtercheque@gmail.com', 'valtercheque', '9bf7e8e20d32d993410ba64364bcf8325b56658a', '3b91b3b2cc99bb4e1a3e115fe39bfdf4f143d346', 0),
(188, 'eltonlaice@hotmail.com', 'eltonlaice', '0f540879547949ad1213a611be742aafdb00036e', '262aaea12f12731a4ef30818ab99ae7f8083ed15', 0),
(189, 'celesteorlando12@gmail.com', 'celesteorlando12', 'c335cca58a6bf449e45e5f525f12a275d948428c', '4887488a48bd062fd5f11dd29a5e9ff8001a89e7', 0),
(190, 'muzimeunice95@gmail.com', 'muzimeunice95', '112c99c809ff5a99aa72ffac6704968533eb8bd5', '39a0149a015d541850828e90739a880fcf114b38', 0),
(191, 'uem.assane@gmail.com', 'uem.assane', 'bede110e80d0380e256970e6d1aa018a4823fef5', '63562d9be6ed5bb2f3d4caae459f43d76b2608c3', 0),
(192, 'vanydina.coutinho@gmail.com', 'vanydina.coutinho', '5d76a2c8fdc5583c3837ce99ff6de12a5267f18d', 'ae3095688015c891867726ee7ade979ecd4ce744', 0),
(193, 'julianelmab@gmail.com', 'julianelmab', '6711353ffd8029e5d5485c7cd3d524f1e8e29788', 'fbb21cea02252d89675d38982b9d0de06736119f', 0),
(194, 'paulozinessa@gmail.com', 'paulozinessa', '3e339b17528bf587ee1222e8600d0c01fc207a6c', 'ae1e41a3545a14d9059b523da05e0e481d79f160', 0),
(195, 'tedyivan255@gmail.com', 'tedyivan255', '6fd9ec898b0f908891bba31dd72ff828b2382bcc', '0b0e52e3592dd02c21164781ca17d909a5fda39b', 0),
(197, 'rubenmanhica@gmail.com', 'rubenmanhica', 'a2d300905dee8d9db067cdaf655792de991a2081', 'cad483387886b1371f1a2a6dbe2a957de7ca26b0', 0),
(198, 'valiissufo@gmail.com', 'valiissufo', '6953d3c2b520dadc3c9f62378700e9082b6b093a', '200341acf5124bfa8c0fc18a08a8f71be1afb59a', 0),
(199, 'tatianakovalenko@gmail.com', 'tatianakovalenko', '552d7cbe563c9ec7e974d725a9c6e32c6cf2ac36', '2de74be4800e861a7f13a7d3ddcf8d2acb8eb309', 0),
(200, 'lourinochemane@gmail.com', 'lourinochemane', '9fdf918f3b9f360d3f32743d6e943114e1db9c75', '3c6b3f89506770318b8952ef3ca40fb9d05458c3', 0),
(201, 'assanecipriano@hotmail.com', 'assanecipriano', 'b88243de103a68f77fb35743bf8942061d53544c', '5cfdf2a717e003fa570b19d190821c64a894d61f', 0),
(202, 'benildojoaquim@gmail.com', 'benildojoaquim', '2d58281a073aabde10282eebc0d8ecfc19cb9a02', 'eb09a3f41309f20ca2d5b2ed612436d3fc1bdd8b', 0),
(203, 'leilaomar@gmail.com', 'leilaomar', 'f199f2e651937ce12cfc28057d75a839ab53794c', '06dcd3034554adffc64000d664691e95a2823682', 0),
(204, 'albinocuinhane@gmail.com', 'albinocuinhaque', '2d0f16f792e03fd54f2a7c6e1ec5e21ac25248cd', 'b819bba5c6446b5c9d78b5de1d6e7f3283ca035a', 0);

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `docente`
--
ALTER TABLE `docente`
  ADD CONSTRAINT `docente_ibfk_1` FOREIGN KEY (`utilizador_id`) REFERENCES `utilizador` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `estudante`
--
ALTER TABLE `estudante`
  ADD CONSTRAINT `estudante_ibfk_1` FOREIGN KEY (`utilizador_id`) REFERENCES `utilizador` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `plano_analitico`
--
ALTER TABLE `plano_analitico`
  ADD CONSTRAINT `plano_analitico_ibfk_1` FOREIGN KEY (`turma_id`) REFERENCES `turmas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `turmas`
--
ALTER TABLE `turmas`
  ADD CONSTRAINT `turmas_ibfk_1` FOREIGN KEY (`curso_id`) REFERENCES `cursos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `turmas_ibfk_2` FOREIGN KEY (`nivel_id`) REFERENCES `niveis` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `turmas_ibfk_3` FOREIGN KEY (`turno_id`) REFERENCES `turnos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
