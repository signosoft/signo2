<?php
    require_once __DIR__ . '/../modelos/exportar.php';
    require_once __DIR__ . '/../repositorios/exportar.php';
    require_once __DIR__ . '/../controladores/exportar.php';

    $repositorio = new UtilizadorRepositorio;
    $utilizadores = array(
      array('id' => null, 'email' => 'edsonmichaque@yandex.com', 'utilizador' => 'edsonmichaque', 'salt' => null, 'password' => 'edsonmichaque', 'desactivado' => false),
      array('id' => null, 'email' => 'densquejamal@gmail.com',  'utilizador' => 'densquejamal', 'salt' => null, 'password' => 'densquejamal', 'desactivado' => false),
      array('id' => null, 'email' => 'albertocremildomoiane@gmail.com', 'utilizador' => 'albertocremildomoiane','salt' => null, 'password' => 'albertocremildomoiane', 'desactivado' => false),
      array('id' => null, 'email' => 'valtercheque@gmail.com',  'utilizador' => 'valtercheque','salt' => null, 'password' => 'valtercheque', 'desactivado' => false),
      array('id' => null, 'email' => 'eltonlaice@hotmail.com',  'utilizador' => 'eltonlaice','salt' => null, 'password' => 'eltonlaice', 'desactivado' => false),
      array('id' => null, 'email' => 'celesteorlando12@gmail.com',  'utilizador' => 'celesteorlando12','salt' => null, 'password' => 'celesteorlando12', 'desactivado' => false),
      array('id' => null, 'email' => 'muzimeunice95@gmail.com',  'utilizador' => 'muzimeunice95','salt' => null, 'password' => 'muzimeunice95', 'desactivado' => false),
      array('id' => null, 'email' => 'uem.assane@gmail.com',  'utilizador' => 'uem.assane','salt' => null, 'password' => 'uem.assane', 'desactivado' => false),
      array('id' => null, 'email' => 'vanydina.coutinho@gmail.com',  'utilizador' => 'vanydina.coutinho','salt' => null, 'password' => 'vanydina.coutinho', 'desactivado' => false),
      array('id' => null, 'email' => 'julianelmab@gmail.com',  'utilizador' => 'julianelmab','salt' => null, 'password' => 'julianelmab', 'desactivado' => false),
      array('id' => null, 'email' => 'paulozinessa@gmail.com', 'utilizador' => 'paulozinessa', 'salt' => null, 'password' => 'paulozinessa', 'desactivado' => false),
      array('id' => null, 'email' => 'tedyivan255@gmail.com',  'utilizador' => 'tedyivan255','salt' => null, 'password' => 'tedyivan255', 'desactivado' => false),
      array('id' => null, 'email' => 'edsonbeats@gmail.com',  'utilizador' => 'edsonbeats','salt' => null, 'password' => 'edsonbeats', 'desactivado' => false),
    );


    foreach ($utilizadores as $u) {
      $senha = new Senha;
      $utilizador = new Utilizador($u['id'], $u['email'], $u['utilizador'], $senha->salt(), $senha->password($u['password']), $u['desactivado']);
      if($repositorio->criar($utilizador)) {
        echo 'O utilizador ' . $utilizador->email() . ' foi criado com sucesso' . "\n";
      } else {
        echo 'O utilizador ' . $utilizador->email() . ' ja existe' . "\n";
      }
    }
?>
