<?php
  require_once __DIR__ . '/Senha.php';

  class Utilizador {
    private $_id;
    private $_email;
    private $_utilizador;
    private $_password;
    private $_salt;
    private $_disactivado;

    public function __construct($id, $email, $utilizador, $salt, $password, $desactivado) {
      $this->_id = $id;
      $this->_email = $email;
      $this->_utilizador = $utilizador;
      $this->_salt = $salt;
      $this->_password = $password;
      $this->_disactivado = $desactivado;
    }

    public static function novo() {
      return new Utilizador(null, null, null, null, null, null);
    }

    public function id() {
      return $this->_id;
    }


    public function email() {
      return $this->_email;
    }

    public function utilizador() {
      return $this->_utilizador;
    }

    public function salt() {
      return $this->_salt;
    }

    public function password() {
      return $this->_password;
    }

    public function desactivado() {
      return $this->_disactivado;
    }

    public function setId($id) {
      $this->_id = $id;
    }


    public function setEmail($email) {
      $this->_email = $email;
    }

    public function setUtilizador($utilizador) {
      $this->_utilizador = $utilizador;
    }

    public function setSalt($salt) {
      $this->_salt = $salt;
    }

    public function setPassword($password) {
      $this->_password = $password;
    }

    public function setDesactivado($desactivado) {
      $this->_disactivado = $desactivado;
    }

  }

?>
