<?php

class Disciplina
{
 	private $_nome;
 	private $_id;

 	function __construct($_id, $_nome)
 	{
 		$this->_nome = $_nome;
 		$this->_id = $_id;
 	}

 	public function id(){
 		return $this->_id;
 	}

 	public function nome(){
 		return $this->_nome;
 	}
}

?>