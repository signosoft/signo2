<?php

require_once __DIR__ . '/CategoriaDeAvaliacao.php';
require_once __DIR__ . '/PlanoDeDisciplina.php';

class Avaliacao{

	private $_id;
	private $_descricao;
	private $_peso;
	private $_data;
	private $_categAval;
	private $_plano;

	public function __construct($_id, $_descricao, $_peso, $_data, CategoriaDeAvaliacao $_categAval, PlanoDeDisciplina $_plano){
		$this->_id = $_id;
		$this->_descricao = $_descricao;
		$this->_peso = $_peso;
		$this->_data = $_data;
		$this->_categAval = $_categAval;
		$this->_plano = $_plano;
	}

	public function id(){
		return $this->_id;
	}

	public function descricao(){
		return $this->_descricao;
	}

	public function peso(){
		return $this->_peso;
	}

	public function data(){
		return $this->_data;
	}

	public function categoria(){
		return $this->_categAval;
	}

	public function plano(){
		return $this->_plano;
	}


}
?>