<?php
  require_once __DIR__ . '/ModeloGenerico.php';

  class Turma {
    private $_id;
    private $_curso;
    private $_nivel;
    private $_turno;

    public function __construct($id, $curso, $nivel, $turno) {
      $this->_id = $id;
      $this->_curso = $curso;
      $this->_nivel = $nivel;
      $this->_turno = $turno;
    }

    public function getId() {
      return $this->_id;
    }


    public function getCurso() {
      return $this->_curso;
    }

    public function getNivel() {
      return $this->_nivel;
    }

    public function getTurno() {
      return $this->_turno;
    }

    public function setId($id) {
      $this->_id = $id;
    }


    public function setCurso($curso) {
      $this->_curso = $curso;
    }

    public function setNivel($nivel) {
      $this->_nivel = $nivel;
    }

    public function setTurno($turno) {
      $this->_turno = $turno;
    }
  }

?>
