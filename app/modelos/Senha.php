<?php
  class Senha {
    private $_salt;
    private $_password;

    public function salt() {
      $this->_salt = sha1(uniqid(mt_rand(), true));
      return $this->_salt;
    }

    public function password($str) {
      return sha1($this->_salt . $str);
    }
  }
?>
