<?php

  class ModeloGenerico {
    protected $_id;
    protected $_nome;
    protected $_tabela;
    protected $_codigo;

    public function __construct($id, $nome, $codigo) {
      $this->_id = $id;
      $this->_nome = $nome;
      $this->_codigo = $codigo;
    }

    public function getId() {
      return $this->_id;
    }

    public function getNome() {
      return $this->_nome;
    }

    public function getTabela() {
      return $this->_tabela;
    }

    public function getCodigo() {
      return $this->_codigo;
    }
  }

  class Nivel extends ModeloGenerico {
    public function __construct($id, $nome, $codigo) {
      parent::__construct($id, $nome, $codigo);
      $this->_tabela = 'niveis';
    }
  }

  class Curso extends ModeloGenerico {
    public function __construct($id, $nome, $codigo) {
      parent::__construct($id, $nome, $codigo);
      $this->_tabela = 'cursos';
    }
  }

  class Turno extends ModeloGenerico {
    public function __construct($id, $nome, $codigo) {
      parent::__construct($id, $nome, $codigo);
      $this->_tabela = 'turnos';
    }
  }

  class Disciplina extends ModeloGenerico {
    public function __construct($id, $nome, $codigo) {
      parent::__construct($id, $nome, $codigo);
      $this->_tabela = 'disciplinas';
    }
  }

?>
