<?php

require_once __DIR__ . '/Disciplina.php';

class PlanoDeDisciplina
{
	private $_id;
	private $_descricao;
	private $_data;
	private $_disciplina;

	function __construct($_id, $_descricao, $_data, Disciplina $_disciplina)
	{
		$this->_id = $_id;
		$this->_descricao = $_descricao;
		$this->_data = $_data;
		$this->_disciplina = $_disciplina;
	}

	function id(){
		return $this->_id;
	}

	function descricao(){
		return $this->_descricao;
	}

	function data(){
		return $this->_data;
	}

	function disciplina(){
		return $this->_disciplina;
	}
}

?>