<?php
 
 class CategoriaDeAvaliacao
 {
 	private $_id;
 	private $_descricao;

 	function __construct($_id, $_descricao)
 	{
 		$this->_id = $_id;
 		$this->_descricao = $_descricao;
 	}

 	public function id(){
 		return $this->_id;
 	}

 	public function descricao(){
 		return $this->_descricao;
 	}
 }
?>