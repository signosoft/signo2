<?php
  require_once __DIR__ . '/Utilizador.php';

  class Docente {
    private $_id;
    private $_nome;
    private $_utilizador;

    public function __construct($id, $nome, Utilizador $utilizador) {
      $this->_id = $id;
      $this->_nome = $nome;
      $this->_utilizador = $utilizador;
    }

    public function id() {
      return $this->_id;
    }

    public function utilizador() {
      return $this->_utilizador;
    }

    public function nome() {
      return $this->_nome;
    }
  }

?>
