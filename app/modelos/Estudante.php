<?php

class Estudante {
  private $_id;
  private $_numero_de_estudante;
  private $_nome;
  private $_utilizador;

  public function __construct($id, $ne, $nome, $u) {
    $this->_id = $id;
    $this->_numero_de_estudante = $ne;
    $this->_nome = $nome;
    $this->_utilizador = $u;
  }

  public function getId() {
    return $this->_id;
  }

  public function getNumeroDeEstudante() {
    return $this->_numero_de_estudante;
  }

  public function getNome() {
    return $this->_nome;
  }

  public function getUtilizador() {
    return $this->_utilizador;
  }
}

?>
