<?php
  require_once __DIR__ . '/importar.php';

  class UtilizadorControlador {
    private $_reporitorio;
    private $_utilizador;

    public function __construct() {
      $this->_repositorio = new UtilizadorRepositorio;
    }

    public function criar(Utilizador $e) {
      $this->_repositorio->criar($e);
    }

    public function autenticar(Utilizador $e) {
      if ($this->_repositorio->existe($e)) {
        $this->_utilizador = $this->_repositorio->encontrar($e->email());
        $salt = $this->_utilizador->salt();
        $pw = $this->_utilizador->password();
        if ($pw == sha1($salt . $e->password())) {
          return $this->_utilizador;
        }

        return false;
      } else {
        return false;
      }
    }

    public function encontrar($email) {
      return $this->_repositorio->encontrar($email);
    }

    public function encontrarPorId($id) {
      return $this->_repositorio->encontrarPorId($id);
    }

    public function eDocente(Utilizador $u) {
      return $this->_repositorio->eDocente($u);
    }

    public function eEstudante(Utilizador $u) {
      return $this->_repositorio->eEstudante($u);
    }
  }
?>