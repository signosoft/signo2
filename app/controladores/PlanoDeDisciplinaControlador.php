<?php
require_once __DIR__ . '/../modelos/exportar.php';

class PlanoDeDisciplinaControlador {
  private $_repositorio;
  private $_plano;

  public function __construct() {
    $this->_repositorio = new PlanoDeDisciplinaRepositorio;
  }

  public function criar($id, $descricao, $data, Disciplina $disciplina) {
    $plano = new PlanoDeDisciplina(null, $descricao, $data, $disciplina);
    $this->_repositorio->criar($plano);
  }

  public function encontrar($str) {
    return $this->_repositorio->encontrar($str);
  }

  public function remover($id) {
    $this->_repositorio->remover($id);
  }
}

?>