<?php
require_once __DIR__ . '/../modelos/exportar.php';

class DocenteControlador {
  private $_repositorio;
  private $_docentes;

  public function __construct() {
    $this->_repositorio = new DocenteRepositorio;
  }

  public function criar($nome, Utilizador $utilizador) {
    $docente = new Docente(null, $nome, $utilizador);
    $this->_repositorio->criar($docente);
  }

  public function remover($id) {
    $this->_repositorio->remover($docente);
  }

  public function todos() {
    return $this->_repositorio->todos();
  }

  public function encontrar($id) {
    return $this->_repositorio($id);
  }

  public function removerTodos() {
    $this->_repositorio->removerTodos();
  }
}

?>