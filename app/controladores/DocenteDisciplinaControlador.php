<?php
require_once __DIR__ . '/../modelos/exportar.php';

class DocenteDisciplinaControlador {
  private $_repositorio;

  public function __construct() {
    $this->_repositorio = new PlanoDeDisciplinaRepositorio;
  }

  public function criar($idDisciplina, $idDocente) {
    $this->_repositorio->criar($idDisciplina, $idDocente);
  }

  public function encontrarPorDocente($idDocente) {
    return $this->_repositorio->encotrarPorDocente($idDocente);
  }

  public function encontrarPorDisciplina($idDisciplina) {
    return $this->_repositorio->encotrarPorDiscplina($idDisciplina);
  }
}

?>