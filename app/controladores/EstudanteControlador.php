<?php
require_once __DIR__ . '/../modelos/exportar.php';
require_once __DIR__ . '/../repositorios/exportar.php';

class EstudanteControlador {
  private $_repositorio;
  private $_docentes;

  public function __construct() {
    $this->_repositorio = new EstudanteRepositorio;
  }

  public function criar($numero_de_cartao, $nome, Utilizador $utilizador) {
    $estudante = new Estudante(null, $numero_de_cartao, $nome, $utilizador);
    $this->_repositorio->criar($estudante);
  }

  /*public function remover($id) {
    $this->_repositorio->remover($docente);
  }

  public function todos() {
    return $this->_repositorio->todos();
  }

  public function encontrar($id) {
    return $this->_repositorio->encontrar($id);
  }

  public function removerTodos() {
    $this->_repositorio->removerTodos();
  }*/
}

?>
