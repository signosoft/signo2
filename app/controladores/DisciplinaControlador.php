<?php
require_once __DIR__ . '/../modelos/exportar.php';

class DisciplinaControlador {
  private $_repositorio;
  private $_disciplina;

  public function __construct() {
    $this->_repositorio = new DisciplinaRepositorio;
  }

  public function criar(Disciplina $disciplina) {
    $this->_repositorio->criar($disciplina);
  }

  public function encontrar($descricao) {
    return $this->_repositorio->encontrar($descricao);
  }

  public function remover($id) {
    $this->_repositorio->remover($id);
  }
}

?>