<?php

  require_once __DIR__ . '/../controladores/exportar.php';
  require_once __DIR__ . '/../modelos/exportar.php';
  require_once __DIR__ . '/renderizar.php';
  require_once __DIR__ . '/../configuracoes/prefixos.php';

  class SessaoControlador {
    public static function login() {
      session_start();
      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $email = $_POST['email'];
        $password = $_POST['password'];
        $uc = new UtilizadorControlador;
        $utilizador = Utilizador::novo();
        $utilizador->setEmail($email);
        $utilizador->setPassword($password);

        $utilizadorAutenticado = $uc->autenticar($utilizador);
        if ($utilizadorAutenticado) {
          $_SESSION['id'] = $utilizadorAutenticado->id();
          if ($uc->eDocente($utilizadorAutenticado)) {
            $_SESSION['tipo'] = 'SIGNO_USER_TYPE:0';
          } else {
            $_SESSION['tipo'] = 'SIGNO_USER_TYPE:1';
          }

          header('location:/index.php/' . $utilizadorAutenticado->utilizador());
        } else {
          $erros = array('Utilizador ou senha incorrectos');
          $title = 'x';
          renderizar('login/index');
        }
      } else {
        if (!isset($_SESSION['id']) && !isset($_SESSION['tipo'])) {
          renderizar('login/index');
        } else {
          self::home();
        }
      }
    }

    public static function logout() {
      session_start();
      if (isset($_SESSION['id'])) {
        unset($_SESSION['id']);
        unset($_SESSION['tipo']);
      }

      header('location:/index.php/login');
    }

    public static function home() {
      $uc = new UtilizadorControlador;
      $categControl = new CategoriasControlador;
      session_start();
      $utilizador = $uc->encontrarPorId($_SESSION['id']);

      if ($uc->eDocente($utilizador)) {
        $_SESSION['id'] = $utilizador->id();
        $_SESSION['tipo'] = 'SIGNO_USER_TYPE:0';
        $categorias = $categControl->todos();
        if($categorias){
          echo "tem algo";
        } else{
          echo "Esta Vazio";
        }

        require_once __DIR__.'/../templates/Docente/home.html';

      } else if ($uc->eEstudante($utilizador)) {
        $_SESSION['id'] = $utilizador->id();
        $_SESSION['tipo'] = 'SIGNO_USER_TYPE:1';
        //require_once __DIR__.'/../templates/Docente/home.html';
      } else {
        $_SESSION['id'] = $utilizador->id();
        $_SESSION['tipo'] = 'SIGNO_USER_TYPE:2';
      }

      
      //header('location:/index.php/' . $utilizador->utilizador());
    }
  }

?>
