<?php
  require_once __DIR__ . '/importar.php';
  require_once __DIR__ . '/BD.php';

  class AvaliacaoRepositorio {
    private $_pdo;
    private $_db;

    public function __construct() {
      $this->_pdo = new BD(HOST, PORT, DATABASE, USER, PASSWORD);
      $this->_db = $this->_pdo->connection();
    }

    public function criar(Avaliacao $a) {
      $sql = 'INSERT INTO avaliacao (descricao,peso,data,categorias_id,planoDisciplina_id) VALUES (:descricao,:peso,:data,:categorias_id,:planoDisciplina_id)';
      if (!$this->existe($a)) {
        $statement = $this->_db->prepare($sql);
      
        $statement->bindParam(':descricao', $a->descricao(), PDO::PARAM_STR, 128);
        $statement->bindParam(':peso', $a->peso(), PDO::PARAM_INT);
        $statement->bindParam(':data', $a->data(), PDO::PARAM_STR, 128);
        $statement->bindParam(':categorias_id', $a->categoria()->id(), PDO::PARAM_INT);
        $statement->bindParam(':planoDisciplina_id', $a->plano()->id(), PDO::PARAM_INT);
        
        $ret = $statement->execute();
        return true;
      } else {
        return false;
      }
    }

    public function existe(Avaliacao $a) {
      $sql = "SELECT idAvaliacao, descricao FROM avaliacao WHERE descricao = :descricao ORDER BY idAvaliacao LIMIT 1";
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':descricao', $a->descricao(), PDO::PARAM_STR, 128);

      $result = $statement->execute();
      $aval = $statement->fetch(PDO::FETCH_ASSOC);

      if ($aval != null) {
        return $aval;
      }

      return false;
    }

    public function encontraPorDisciplina($planoDisciplina_id){
      $sql = "SELECT idAvaliacao,descricao,peso,data,categorias_id,planoDisciplina_id FROM avaliacao WHERE planoDisciplina_id = :planoDisciplina_id";

      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':planoDisciplina_id',$planoDisciplina_id,PDO::PARAM_INT);
      $statement->execute();
      $aval = $statement->fetchAll();

      if($aval){
        return $aval;
      } else{
        return false;
      }

    }

   public function encontrar($str) {
      $sql = "SELECT idAvaliacao, descricao FROM avaliacao WHERE descricao LIKE :str";
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':str', '%'.$str.'%', PDO::PARAM_STR, 128);

      $result = $statement->execute();
      $aval = $statement->fetch(PDO::FETCH_ASSOC);

      if ($aval != null) {
        return $aval;
      }

      return false;
    }

    public function removerTodos() {
      $sql = 'DELETE FROM avaliacao';
      $this->_db->exec($sql);
    }
  }
  

  $av = new AvaliacaoRepositorio;
  $avas = $av->encontraPorDisciplina(2);

  if($avas){
    echo "".$avas['descricao'];
}


?>