<?php

require_once __DIR__ . '/importar.php';

  class DisciplinaRepositorio {
    private $_pdo;
    private $_db;

    public function __construct() {
      $this->_pdo = new BD(HOST, PORT, DATABASE, USER, PASSWORD);
      $this->_db = $this->_pdo->connection();
    }

    public function criar(Disciplina $a) {
      $sql = 'INSERT INTO disciplina (descricao) VALUES (:descricao)';
      if (!$this->encontrar($a->nome())) {
        $statement = $this->_db->prepare($sql);
      
        $statement->bindParam(':descricao', $a->nome(), PDO::PARAM_STR, 128);
        
        $ret = $statement->execute();
        return true;
      } else {
        return false;
      }
    }

    public function encontrar($str){
	    $sql = "SELECT id,descricao FROM disciplina where descricao LIKE ? ORDER BY id LIMIT 1";

      $statement = $this->_db->prepare($sql);
      $statement->bindValue(1,"%$str%", PDO::PARAM_STR);
      $statement->execute();

      $dados = array();
      
      if (!$statement->rowCount() == 0) {
        $result = $statement->fetch();
        $dados = ['id'=>$result['id'],'descricao'=>$result['descricao']];
        return $dados;  
      } else{
        return false;
      }
      }

    }

?>