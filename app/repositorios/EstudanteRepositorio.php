<?php

 require_once __DIR__ . '/importar.php';

 class EstudanteRepositorio {
 	private $_pdo;
 	private $_db;

 	public function __construct() {
 		$this->_pdo = new BD(HOST, PORT, DATABASE, USER, PASSWORD);
 		$this->_db = $this->_pdo->connection();
 	}

 	public function criar(Estudante $e){
 		$sql = 'INSERT INTO estudante (numero_de_estudante, nome, utilizador_id) VALUES (:numero_de_estudante, :nome, :utilizador_id)';

    $statement = $this->_db->prepare($sql);
    $statement->bindParam(':numero_de_estudante', $e->getNumeroDeEstudante(), PDO::PARAM_STR, 128);
    $statement->bindParam(':nome', $e->getNome(), PDO::PARAM_STR, 128);
    $statement->bindParam(':utilizador_id', $e->getUtilizador()->id(), PDO::PARAM_INT);

    $ret = $statement->execute();
    return true;
  }

  public function existe(Utilizador $e) {
    $sql = "SELECT * utilizador_id FROM estudante WHERE utilizador_id = :utilizador_id ORDER BY id LIMIT 1";
    $statement = $this->_db->prepare($sql);
    $statement->bindParam(':utilizador_id', $e->id());

    $result = $statement->execute();
    $user = $statement->fetch(PDO::FETCH_ASSOC);

    if ($user != null) {
      return true;
    }

    return false;
  }

  /*public function encontrar(Utilizador $e) {
    $sql = "SELECT * FROM estudante WHERE utilizador_id = :uid ORDER BY id LIMIT 1";
    $statement = $this->_db->prepare($sql);
    $statement->bindParam(':utilizador_id', $e->id());
    $result = $statement->execute();

    $estudante = $statement->fetch(PDO::FETCH_ASSOC);

    if ($user != null) {
      $id = $estudante['id'];
      $nc = $estudante['numero_de_estudante'];
      $nome = $estudante['nome'];
      $_docente = new Estudante($id, $nc, $nome, $e);

      return $_estudante;
    }

    return false;
  }*/

  public function removerTodos() {
    $sql = 'DELETE FROM estudante';
    $this->_db->exec($sql);
  }
}

?>
