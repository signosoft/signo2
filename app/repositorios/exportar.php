<?php
  require_once __DIR__ . '/UtilizadorRepositorio.php';
  require_once __DIR__ . '/DocenteRepositorio.php';
  require_once __DIR__ . '/EstudanteRepositorio.php';
  require_once __DIR__ . '/CategoriaDeAvaliacaoRepositorio.php';
  require_once __DIR__ . '/PlanoDeDisciplinaRepositorio.php';
  require_once __DIR__ . '/DisciplinaRepositorio.php';
  require_once __DIR__ . '/AvaliacaoRepositorio.php';
  require_once __DIR__ . '/DocenteDiciplinaRepositorio.php';
  require_once __DIR__ . '/RepositorioGenerico.php';
  require_once __DIR__ . '/TurmaRepositorio.php';
  require_once __DIR__ . '/BD.php';
?>
