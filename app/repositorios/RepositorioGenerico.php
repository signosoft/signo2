<?php

 require_once __DIR__ . '/importar.php';

 class RepositorioGenerico {
 	private $_pdo;
 	private $_db;

 	public function __construct() {
    $this->_pdo = new BD(HOST, PORT, DATABASE, USER, PASSWORD);
 		$this->_db = $this->_pdo->connection();
 	}

 	public function criar(ModeloGenerico $e){
    $sql = "INSERT INTO {$e->getTabela()} (nome, codigo) VALUES (:nome, :codigo)";
    $statement = $this->_db->prepare($sql);
    $statement->bindParam(':nome', $e->getNome(), PDO::PARAM_STR, 128);
    $statement->bindParam(':codigo', $e->getCodigo(), PDO::PARAM_STR, 128);

    $ret = $statement->execute();
    return true;
  }


  public function todos($tabela) {
    $sql = "SELECT * FROM $tabela";
    $statement = $this->_db->prepare($sql);
    $ret = $statement->execute();

    if ($ret) {
      $lista = array();
      while($retorno = $statement->fetch(PDO::FETCH_OBJ)) {
        if ($tabela === 'cursos') {
          array_push($lista, new Curso($retorno->id, $retorno->nome, $retorno->codigo));
        } else if ($tabela === 'turnos') {
          array_push($lista, new Turno($retorno->id, $retorno->nome, $retorno->codigo));
        } else if ($tabela === 'niveis') {
          array_push($lista, new Nivel($retorno->id, $retorno->nome, $retorno->codigo));
        } else if ($tabela === 'disciplinas') {
          array_push($lista, new Disciplina($retorno->id, $retorno->nome, $retorno->codigo));
        } else {
          return false;
        }
      }

      return $lista;
    } else {
      return false;
    }
  }

  public function removerTodos($tabela) {
    $sql = "DELETE FROM $tabela";
    $this->_db->exec($sql);
  }

  public function encontrarPorId($id, $tabela) {
    $sql = "SELECT * FROM $tabela WHERE id = :id ORDER BY id LIMIT 1";
    $statement = $this->_db->prepare($sql);
    $statement->bindParam(':id', $id, PDO::PARAM_INT);
    $result = $statement->execute();
    if ($result) {
      $s = $statement->fetch(PDO::FETCH_OBJ);
      if ($tabela === 'cursos') {
        return new Curso($s->id, $s->nome, $s->codigo);
      } else if ($tabela === 'turnos') {
        return new Turno($s->id, $s->nome, $s->codigo);
      } else if ($tabela === 'niveis') {
        return new Nivel($s->id, $s->nome, $s->codigo);
      } else {
        return false;
      }
    }
  }

  public function encontrarPorCodigo($id, $tabela) {
    $sql = "SELECT * FROM $tabela WHERE codigo = :id ORDER BY id LIMIT 1";
    $statement = $this->_db->prepare($sql);
    $statement->bindParam(':id', $id, PDO::PARAM_INT);
    $result = $statement->execute();
    if ($result) {
      $s = $statement->fetch(PDO::FETCH_OBJ);
      if ($tabela === 'cursos') {
        return new Curso($s->id, $s->nome, $s->codigo);
      } else if ($tabela === 'turnos') {
        return new Turno($s->id, $s->nome, $s->codigo);
      } else if ($tabela === 'niveis') {
        return new Nivel($s->id, $s->nome, $s->codigo);
      } else if ($tabela === 'disciplinas') {
        return new Disciplina($s->id, $s->nome, $s->codigo);
      } else {
        return false;
      }
    }
  }

  public function ligarDisciplinaATurma($disciplina, $turma) {
    $sql = "INSERT INTO disciplina_turma (disciplina_id, turma_id) VALUES (:disciplina_id, :turma_id)";
    $statement = $this->_db->prepare($sql);
    $statement->bindParam(':disciplina_id', $disciplina->getId(), PDO::PARAM_INT);
    $statement->bindParam(':turma_id', $turma->getId(), PDO::PARAM_INT);
    return $statement->execute();
  }

  public function encontrarTurmasDaDisciplina($disciplina) {
    $rep = new TurmaRepositorio;
    $sql = "SELECT  turma_id FROM disciplina_turma WHERE disciplina_id = :disciplina_id";
    $statement = $this->_db->prepare($sql);
    $statement->bindParam(':disciplina_id', $disciplina->getId(), PDO::PARAM_INT);

    $ret = $statement->execute();

    if ($ret) {
      $lista = array();
      while($retorno = $statement->fetch(PDO::FETCH_OBJ)) {
        array_push($lista, $rep->encontrarPorId($retorno->id));
      }

      return $lista;
    }

    return false;
  }

  public function encontrarDisciplinasDaTurma($turma) {
    $rep = new RepositorioGenerico;
    $sql = "SELECT  disciplina_id FROM disciplina_turma WHERE turma_id = :turma_id";
    $statement = $this->_db->prepare($sql);
    $statement->bindParam(':disciplina_id', $turma->getId(), PDO::PARAM_INT);

    $ret = $statement->execute();

    if ($ret) {
      $lista = array();
      while($retorno = $statement->fetch(PDO::FETCH_OBJ)) {
        array_push($lista, $this->encontrarPorId($retorno->id, 'disciplinas'));
      }

      return $lista;
    }

    return false;
  }

}

?>
