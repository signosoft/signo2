<?php
  require_once __DIR__ . '/importar.php';

  class DocenteRepositorio {
    private $_pdo;
    private $_db;

    public function __construct() {
      $this->_pdo = new BD(HOST, PORT, DATABASE, USER, PASSWORD);
      $this->_db = $this->_pdo->connection();
    }

    public function criar(Docente $e) {
      $sql = 'INSERT INTO docente (nome, utilizador_id) VALUES (:nome, :utilizador_id)';
      if (!$this->existe($e->utilizador())) {
        $statement = $this->_db->prepare($sql);
      
        $statement->bindParam(':nome', $e->nome(), PDO::PARAM_STR, 128);
        $statement->bindParam(':utilizador_id', $e->utilizador()->id(), PDO::PARAM_INT);

        $ret = $statement->execute();
        return true;
      } else {
        return false;
      }
    }

    public function existe(Utilizador $e) {
      $sql = "SELECT id, nome, utilizador_id FROM docente WHERE utilizador_id = :utilizador_id ORDER BY id LIMIT 1";
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':utilizador_id', $e->id());

      $result = $statement->execute();
      $user = $statement->fetch(PDO::FETCH_ASSOC);

      if ($user != null) {
        return true;
      }

      return false;
    }

    public function encontrar(Utilizador $e) {
      $sql = "SELECT * FROM docente WHERE utilizador_id = :uid ORDER BY id LIMIT 1";
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':utilizador_id', $e->id());
      $result = $statement->execute();

      $docente = $statement->fetch(PDO::FETCH_ASSOC);

      if ($user != null) {
        $id = $docente['id'];
        $nome = $docente['nome'];
        $_docente = new Docente($id, $nome, $e);

        return $_docente;
      }

      return false;
    }

    public function removerTodos() {
      $sql = 'DELETE FROM docente';
      $this->_db->exec($sql);
    }
  }
?>
