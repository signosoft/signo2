<?php

require_once __DIR__ . '/importar.php';

  class PlanoDeDisciplinaRepositorio {
    private $_pdo;
    private $_db;

    public function __construct() {
      $this->_pdo = new BD(HOST, PORT, DATABASE, USER, PASSWORD);
      $this->_db = $this->_pdo->connection();
    }

    public function criar(PlanoDeDisciplina $a) {
      $sql = 'INSERT INTO planodisciplina (descricao,data,disciplina_id) VALUES (:descricao,:data,:disciplina_id)';
      if (!$this->encontrar($a->descricao())) {
        $statement = $this->_db->prepare($sql);
      
        $statement->bindParam(':descricao', $a->descricao(), PDO::PARAM_STR, 128);
        $statement->bindParam(':data', $a->data(), PDO::PARAM_STR, 128);
        $statement->bindParam(':disciplina_id', $a->disciplina()->id(), PDO::PARAM_INT);
        
        $ret = $statement->execute();
        return true;
      } else {
        return false;
      }
    }

    public function encontrar($str){
      $sql = 'SELECT id,descricao,data,disciplina_id FROM planodisciplina WHERE descricao LIKE ? ORDER BY id LIMIT 1';
     
        $statement = $this->_db->prepare($sql);
      
        $statement->bindValue(1,"$str", PDO::PARAM_STR);
        $statement->execute();

        $dados = array();
      
      if (!$statement->rowCount() == 0) {
        $result = $statement->fetch();
        $dados = ['id'=>$result['id'],'descricao'=>$result['descricao'], 'data'=>$result['data'], 'disciplina_id'=>$result['disciplina_id']];
        return $dados;  
      } else{
        return false;
      }
      
    }

    public function encontrarPorDisciplina($disciplina_id){
    	$sql = 'SELECT id,descricao,data,disciplina_id FROM planodisciplina WHERE id=:id';
      if (!$this->existe($a)) {
        $statement = $this->_db->prepare($sql);
      
        $statement->bindParam(':id', $disciplina_id, PDO::PARAM_INT);
        $ret = $statement->execute();

        $dados = array();
      
      if (!$statement->rowCount() == 0) {
        $result = $statement->fetch();
        $dados = ['id'=>$result['id'],'descricao'=>$result['descricao'], 'data'=>$result['data'], 'disciplina_id'=>$result['disciplina_id']];
        return $dados;  
      } else{
        return false;
      }
      }
    }
  }
/*
    $disciplina = new DisciplinaRepositorio;
    $dis = $disciplina->encontrar('AMI');
    $disci = new Disciplina($dis['id'],$dis['descricao']);

    $plano = new PlanoDeDisciplina(null,'planoAMI',null,$disci);
    $prep = new PlanoDeDisciplinaRepositorio;
    $prep->criar($plano);
*/

?>