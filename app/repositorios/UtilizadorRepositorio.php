<?php
  require_once __DIR__ . '/importar.php';

  class UtilizadorRepositorio {
    private $_pdo;
    private $_db;

    public function __construct() {
      $this->_pdo = new BD(HOST, PORT, DATABASE, USER, PASSWORD);
      $this->_db = $this->_pdo->connection();
    }

    public function criar(Utilizador $e) {
      $sql = 'INSERT INTO utilizador (email, utilizador, password, salt, desactivado) VALUES (:email, :utilizador, :password, :salt, :desactivado)';
      if (!$this->existe($e)) {
        $statement = $this->_db->prepare($sql);
        $statement->bindParam(':email', $e->email(), PDO::PARAM_STR, 128);
        $statement->bindParam(':utilizador', $e->utilizador(), PDO::PARAM_STR, 128);
        $statement->bindParam(':password', $e->password(), PDO::PARAM_STR, 128);
        $statement->bindParam(':salt', $e->salt(), PDO::PARAM_STR, 128);
        $statement->bindParam(':desactivado', $e->desactivado(), PDO::PARAM_INT);

        $ret = $statement->execute();
        return true;
      } else {
        return false;
      }
    }

    public function existe(Utilizador $e) {
      $sql = "SELECT id, email FROM utilizador WHERE email = :email ORDER BY id LIMIT 1";
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':email', $e->email());
      $result = $statement->execute();

      $user = $statement->fetch(PDO::FETCH_ASSOC);

      if ($user != null) {
        return true;
      }

      return false;
    }

    public function encontrar($email) {
      $sql = "SELECT * FROM utilizador WHERE email = :email ORDER BY id LIMIT 1";
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':email', $email);
      $result = $statement->execute();

      $user = $statement->fetch(PDO::FETCH_ASSOC);

      if ($user != null) {
        $id = $user['id'];
        $email = $user['email'];
        $utilizador = $user['utilizador'];
        $salt = $user['salt'];
        $password = $user['password'];
        $desactivado = $user['desactivado'];
        $_user = new Utilizador($id, $email, $utilizador, $salt, $password, $desactivado);

        return $_user;
      }

      return false;
    }

    public function encontrarPorId($id) {
      $sql = "SELECT * FROM utilizador WHERE id = :id ORDER BY id LIMIT 1";
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':id', $id, PDO::PARAM_INT);
      $result = $statement->execute();

      $user = $statement->fetch(PDO::FETCH_ASSOC);

      if ($user != null) {
        $id = $user['id'];
        $email = $user['email'];
        $utilizador = $user['utilizador'];
        $salt = $user['salt'];
        $password = $user['password'];
        $desactivado = $user['desactivado'];
        $_user = new Utilizador($id, $email, $utilizador, $salt, $password, $desactivado);

        return $_user;
      }

      return false;
    }

    public function eDocente(Utilizador $u) {
      $sql = "SELECT * FROM docente WHERE utilizador_id = :id ORDER BY id LIMIT 1";
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':id', $u->id(), PDO::PARAM_INT);
      $result = $statement->execute();

      $docente = $statement->fetch(PDO::FETCH_ASSOC);

      if ($docente != null) {
        $id = $docente['id'];
        $nome = $docente['nome'];
        $_docente = new Docente($id, $nome, $u);

        return $_docente;
      }

      return false;
    }

    public function eEstudante(Utilizador $u) {
      $sql = "SELECT * FROM estudante WHERE utilizador_id = :id ORDER BY id LIMIT 1";
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':id', $u->id(), PDO::PARAM_INT);
      $result = $statement->execute();

      $estudante = $statement->fetch(PDO::FETCH_ASSOC);

      if ($estudante != null) {
        $id = $estudante['id'];
        $nome = $estudante['nome'];
        $ne = $estudante['numero_de_estudante'];
        $_estudante = new Estudante($id, $ne, $nome, $u);

        return $_estudante;
      }

      return false;
    }

    public function removerTodos() {
      $sql = 'DELETE FROM utilizador';
      $this->_db->exec($sql);
    }
  }
?>
