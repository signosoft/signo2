<?php
  require_once __DIR__ . '/importar.php';

  class CategoriaDeAvaliacaoRepositorio {
    private $_pdo;
    private $_db;

    public function __construct() {
      $this->_pdo = new BD(HOST, PORT, DATABASE, USER, PASSWORD);
      $this->_db = $this->_pdo->connection();
    }

    public function criar(CategoriaDeAvaliacao $c) {
      $sql = 'INSERT INTO categorias (descricao) VALUES (:descricao)';
      if (!$this->existe($c)) {
        $statement = $this->_db->prepare($sql);
      
        $statement->bindParam(':descricao', $c->descricao(), PDO::PARAM_STR, 128);
        
        $ret = $statement->execute();
        return true;
      } else {
        return false;
      }
    }

    public function existe(CategoriaDeAvaliacao $c) {
      $sql = "SELECT id, descricao FROM categorias WHERE descricao = :descricao ORDER BY id LIMIT 1";
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':descricao', $c->descricao(), PDO::PARAM_STR, 128);

      $result = $statement->execute();
      $categ = $statement->fetch(PDO::FETCH_ASSOC);

      if ($categ != null) {
        return $categ;
      }

      return false;
    }

    public function encontrar($str){
      $sql = "SELECT id,descricao FROM categorias where descricao LIKE ? ORDER BY id LIMIT 1";

      $statement = $this->_db->prepare($sql);
      $statement->bindValue(1,"%$str%", PDO::PARAM_STR);
      $statement->execute();

      $dados = array();
      
      if (!$statement->rowCount() == 0) {
        $result = $statement->fetch();
        $dados = ['id'=>$result['id'],'descricao'=>$result['descricao']];
        return $dados;  
      } else{
        return false;
      }

    }

    public function todos(){
      $sql = "SELECT id,descricao FROM categorias";

      $statement = $this->_db->prepare($sql);
      $statement->execute();
      $result = $statement->fetchAll();
      if($result){
        return $result;
      } else {
        return false;
      }
        
    }

    public function removerTodos() {
      $sql = 'DELETE FROM categorias';
      $this->_db->exec($sql);
    }
  }

  $ct = new CategoriaDeAvaliacaoRepositorio;

  $dad = $ct->todos();

  if($dad){
    echo 'YESSS';
  }

?>
