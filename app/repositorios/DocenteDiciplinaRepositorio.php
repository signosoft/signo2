<?php

require_once __DIR__ . '/importar.php';

  class DocenteDiciplinaRepositorio {
    private $_pdo;
    private $_db;

    public function __construct() {
      $this->_pdo = new BD(HOST, PORT, DATABASE, USER, PASSWORD);
      $this->_db = $this->_pdo->connection();
    }

    public function criar($planodisciplina_id, $docente_id) {
      $sql = 'INSERT INTO docente_has_planodisciplina(docente_id,planoDisciplina_id) VALUES (:docente_id,:planoDisciplina_id)';
 
        $statement = $this->_db->prepare($sql);
      
        $statement->bindParam(':docente_id', $docente_id, PDO::PARAM_INT);
        $statement->bindParam(':planoDisciplina_id', $planodisciplina_id, PDO::PARAM_INT);
        
        $ret = $statement->execute();
        return true;
    }

    public function encontrarPorDocente($docente_id){
    	$sql = "SELECT * FROM docente_has_planodisciplina WHERE docente_id = :docente_id ORDER BY id LIMIT 1";
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':docente_id', $id, PDO::PARAM_INT);
      $statement->execute();
      
      if (!$statement->rowCount() == 0) {
        $result = $statement->fetch();
        $dados = ['docente_id'=>$result['docente_id'],'planoDisciplina_id'=>$result['planoDisciplina_id']];
        return $dados;  
      } else{
        return false;
      }
    }
      
   public function encontrarPorDisciplina($disciplina_id){
    	$sql = 'SELECT docente_id,planoDisciplina_id FROM docente_has_planodisciplina WHERE planoDisciplina_id=:planoDisciplina_id';
    	$statement = $this->_db->prepare($sql);
      
        $statement->bindParam(':planoDisciplina_id', $disciplina_id, PDO::PARAM_INT);
        $statement->execute();
       	$dados = array();
      
      	if (!$statement->rowCount() == 0) {
        	$result = $statement->fetch();
        	$dados = ['docente_id'=>$result['docente_id'],'planoDisciplina_id'=>$result['planoDisciplina_id']];
        	return $dados;  
      	} else{
        	return false;
      	}

    }
  }
/*
    $rep = new DocenteDiciplinaRepositorio;
    $d = $rep->encontrarPorDisciplina(0);
    if($d){
      echo "Docente_id--->".$d['docente_id'];
    }else{
      echo "Falhou";
    }
    */
    
?>