<?php
  require_once __DIR__ . '/importar.php';

  class TurmaRepositorio {
    private $_pdo;
    private $_db;

    public function __construct() {
      $this->_pdo = new BD(HOST, PORT, DATABASE, USER, PASSWORD);
      $this->_db = $this->_pdo->connection();
    }

    public function criar(Turma $e) {
      $sql = 'INSERT INTO turmas (curso_id, nivel_id, turno_id) VALUES (:curso_id, :nivel_id, :turno_id)';
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':curso_id', $e->getCurso()->getId(), PDO::PARAM_INT);
      $statement->bindParam(':nivel_id', $e->getNivel()->getId(), PDO::PARAM_INT);
      $statement->bindParam(':turno_id', $e->getTurno()->getId(), PDO::PARAM_INT);

      $ret = $statement->execute();
    }

    public function todasTurmas() {
      $sql = 'SELECT * FROM turmas';
      $statement = $this->_db->prepare($sql);

      $ret = $statement->execute();
      if ($ret) {
        $turmas = array();
        while($t = $statement->fetch(PDO::FETCH_OBJ)) {
          $rep = new RepositorioGenerico;
          $curso = $rep->encontrarPorId($t->curso_id, 'cursos');
          $turno = $rep->encontrarPorId($t->turno_id, 'turnos');
          $nivel = $rep->encontrarPorId($t->nivel_id, 'niveis');
          $turma = new Turma($t->id, $curso, $nivel, $turno);

          array_push($turmas, $turma);
        }

        return $turmas;
      } else {
        return false;
      }
    }

    public function encontrarPorId($id) {
      $sql = 'SELECT * FROM turmas WHERE id = :id LIMIT 1';
      $statement = $this->_db->prepare($sql);

      $ret = $statement->execute();
      if ($ret) {
        $t = $statement->fetch(PDO::FETCH_OBJ);
        $rep = new RepositorioGenerico;
        $curso = $rep->encontrarPorId($t->curso_id, 'cursos');
        $turno = $rep->encontrarPorId($t->turno_id, 'turnos');
        $nivel = $rep->encontrarPorId($t->nivel_id, 'niveis');
        $turma = new Turma($t->id, $curso, $nivel, $turno);

        return $turma;
      }

      return false;
    }

    public function todasTurmasPorCurso($curso) {
      $sql = 'SELECT * FROM turmas WHERE curso_id = :curso_id';
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':curso_id', $curso->getId(), PDO::PARAM_INT);

      $ret = $statement->execute();
      if ($ret) {
        $turmas = array();
        while($t = $statement->fetch(PDO::FETCH_OBJ)) {
          $rep = new RepositorioGenerico;
          $curso = $rep->encontrarPorId($t->curso_id, 'cursos');
          $turno = $rep->encontrarPorId($t->turno_id, 'turnos');
          $nivel = $rep->encontrarPorId($t->nivel_id, 'niveis');
          $turma = new Turma($t->id, $curso, $nivel, $turno);

          array_push($turmas, $turma);
        }

        return $turmas;
      } else {
        return false;
      }
    }

    public function todasTurmasPorNivel($nivel_id) {
      $sql = 'SELECT * FROM turmas WHERE nivel_id = :nivel_id';
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':nivel_id', $nivel_id, PDO::PARAM_INT);

      $ret = $statement->execute();
      if ($ret) {
        $turmas = array();
        while($t = $statement->fetch(PDO::FETCH_OBJ)) {
          $rep = new RepositorioGenerico;
          $curso = $rep->encontrarPorId($t->curso_id, 'cursos');
          $turno = $rep->encontrarPorId($t->turno_id, 'turnos');
          $nivel = $rep->encontrarPorId($t->nivel_id, 'niveis');
          $turma = new Turma($t->id, $curso, $nivel, $turno);

          array_push($turmas, $turma);
        }

        return $turmas;
      } else {
        return false;
      }
    }

    public function todasTurmasPorTurno($turno_id) {
      $sql = 'SELECT * FROM turmas WHERE turno_id = :turno_id';
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':turno_id', $turno_id, PDO::PARAM_INT);

      $ret = $statement->execute();
      if ($ret) {
        $turmas = array();
        while($t = $statement->fetch(PDO::FETCH_OBJ)) {
          $rep = new RepositorioGenerico;
          $curso = $rep->encontrarPorId($t->curso_id, 'cursos');
          $turno = $rep->encontrarPorId($t->turno_id, 'turnos');
          $nivel = $rep->encontrarPorId($t->nivel_id, 'niveis');
          $turma = new Turma($t->id, $curso, $nivel, $turno);

          array_push($turmas, $turma);
        }

        return $turmas;
      } else {
        return false;
      }
    }

    public function todasTurmasPorCusroNivelTurno($curso_id, $nivel_id, $turno_id) {
      $sql = 'SELECT * FROM turmas WHERE curso_id = :curso_id AND nivel_id = :nivel_id AND turno_id = :turno_id LIMIT 1';
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':curso_id', $curso_id, PDO::PARAM_INT);
      $statement->bindParam(':nivel_id', $nivel_id, PDO::PARAM_INT);
      $statement->bindParam(':turno_id', $turno_id, PDO::PARAM_INT);
      $ret = $statement->execute();
      if ($ret) {
        while($t = $statement->fetch(PDO::FETCH_OBJ)) {
          $rep = new RepositorioGenerico;
          $curso = $rep->encontrarPorId($t->curso_id, 'cursos');
          $turno = $rep->encontrarPorId($t->turno_id, 'turnos');
          $nivel = $rep->encontrarPorId($t->nivel_id, 'niveis');
          $turma = new Turma($t->id, $curso, $nivel, $turno);
          return $turma;
        }
      } else {
        return false;
      }
    }

    public function todasTurmaPorCodigos($codigos) {
      $curso = $codigos['curso'];
      $nivel = $codigos['nivel'];
      $turno = $codigos['turno'];
      $rep = new RepositorioGenerico;
      $curso_obj = $rep->encontrarPorCodigo($curso, 'cursos');
      $turno_obj = $rep->encontrarPorCodigo($turno, 'turnos');
      $nivel_obj = $rep->encontrarPorCodigo($nivel, 'niveis');

      if ($curso_obj === false || $turno_obj === false || $nivel_obj === false) {
        return false;
      }

      $sql = 'SELECT * FROM turmas WHERE curso_id = :curso_id AND nivel_id = :nivel_id AND turno_id = :turno_id LIMIT 1';
      $statement = $this->_db->prepare($sql);
      $statement->bindParam(':curso_id', $curso_obj->getId(), PDO::PARAM_INT);
      $statement->bindParam(':nivel_id', $nivel_obj->getId(), PDO::PARAM_INT);
      $statement->bindParam(':turno_id', $turno_obj->getId(), PDO::PARAM_INT);
      $ret = $statement->execute();
      if ($ret) {
        while($t = $statement->fetch(PDO::FETCH_OBJ)) {
          $turma = new Turma($t->id, $curso_obj, $nivel_obj, $turno_obj);
          return $turma;
        }
      } else {
        return false;
      }
    }

    public function removerTodas() {
      $sql = 'DELETE FROM turmas';
      $statement = $this->_db->prepare($sql);

      $ret = $statement->execute();
    }

    // Os metodos daqui para baixo tem de ir a um controlador (de turmas)
    public function turmaDoInformaticaNivel1Diurno() {
      return $this->todasTurmaPorCodigos(array('curso' => 'INFORMATICA', 'turno' => 'DIURNO', 'nivel' => 'NIVEL1'));
    }

    public function turmaDoInformaticaNivel2Diurno() {
      return $this->todasTurmaPorCodigos(array('curso' => 'INFORMATICA', 'turno' => 'DIURNO', 'nivel' => 'NIVEL2'));
    }

    public function turmaDoInformaticaNivel3Diurno() {
      return $this->todasTurmaPorCodigos(array('curso' => 'INFORMATICA', 'turno' => 'DIURNO', 'nivel' => 'NIVEL3'));
    }

    public function turmaDoInformaticaNivel4Diurno() {
      return $this->todasTurmaPorCodigos(array('curso' => 'INFORMATICA', 'turno' => 'DIURNO', 'nivel' => 'NIVEL4'));
    }

    public function turmaDoInformaticaNivel5Diurno() {
      return $this->todasTurmaPorCodigos(array('curso' => 'INFORMATICA', 'turno' => 'DIURNO', 'nivel' => 'NIVEL5'));
    }

    public function turmaDoInformaticaNivel1Nocturno() {
      return $this->todasTurmaPorCodigos(array('curso' => 'INFORMATICA', 'turno' => 'NOCTURNO', 'nivel' => 'NIVEL1'));
    }

    public function turmaDoInformaticaNivel2Nocturno() {
      return $this->todasTurmaPorCodigos(array('curso' => 'INFORMATICA', 'turno' => 'NOCTURNO', 'nivel' => 'NIVEL2'));
    }

    public function turmaDoInformaticaNivel3Nocturno() {
      return $this->todasTurmaPorCodigos(array('curso' => 'INFORMATICA', 'turno' => 'NOCTURNO', 'nivel' => 'NIVEL3'));
    }

    public function turmaDoInformaticaNivel4Nocturno() {
      return $this->todasTurmaPorCodigos(array('curso' => 'INFORMATICA', 'turno' => 'NOCTURNO', 'nivel' => 'NIVEL4'));
    }

    public function turmaDoInformaticaNivel5Nocturno() {
      return $this->todasTurmaPorCodigos(array('curso' => 'INFORMATICA', 'turno' => 'NOCTURNO', 'nivel' => 'NIVEL5'));
    }

    public function turma($codigos) {
      return $this->todasTurmaPorCodigos($codigos);
    }

  }

?>
