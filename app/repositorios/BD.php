<?php
  class BD {
    private $_host;
    private $_port;
    private $_database;
    private $_user;
    private $_password;
    private $_instance;

    public function __construct($host, $port, $db, $user, $password) {
      try {
        $this->_instance = new PDO("mysql:host=$host;port=$port;dbname=$db", "$user", "$password", array(PDO::ATTR_PERSISTENT => true));
        $this->_instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      } catch (PDOException $e) {
        die($e->getMessage());
      }
    }

    public function connection() {
      return $this->_instance;
    }
  }
?>
