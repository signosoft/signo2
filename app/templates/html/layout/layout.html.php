<!DOCTYPE html>
<html>
  <head>
    <title><?php echo "title"; ?></title>
    <link rel="stylesheet" href="../../publico/js/aplicacao.css" type="text/css" />
    <script src="../../publico/js/aplicacao.js"></script>
  </head>
  <body>
    <?php require_once __DIR__ . '/header.html.php'; ?>
    <?php require_once  "$body"; ?>
    <?php require_once __DIR__ . '/footer.html.php'; ?>
  </body>
</html>
