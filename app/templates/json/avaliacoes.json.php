<?php
  $total = count($avaliacoes);
  $i = 0;
?>

{
  "turnos" : [
    <?php foreach($avaliacoes as $avaliacao): ?>
      {
        "<?php echo "id"; ?>":<?php echo $avaliacao->getId(); ?>",
        "<?php echo "nome"; ?>":<?php echo $avaliacao->getNome(); ?>",
        <?php $i++; ?>
      }
      <?php if($total != $i ) : ?>
        ,
      <?php endif; :?>
    <?php endforeach; ?>
  ]
}
