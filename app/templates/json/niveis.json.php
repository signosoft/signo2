<?php
  $total = count($turnos);
  $i = 0;
?>

{
  "turnos" : [
    <?php foreach($niveis as $nivel): ?>
      {
        "<?php echo "id"; ?>":<?php echo $nivel->getId(); ?>",
        "<?php echo "nome"; ?>":<?php echo $nivel->getNome(); ?>",
        <?php $i++; ?>
      }
      <?php if($total != $i ) : ?>
        ,
      <?php endif; :?>
    <?php endforeach; ?>
  ]
}
