<?php
  $totalDeTurnos = count($turnos);
  $i = 0;
?>

{
  "turnos" : [
    <?php foreach($turnos as $turno): ?>
      {
        "<?php echo "id"; ?>":<?php echo $turno->getId(); ?>",
        "<?php echo "nome"; ?>":<?php echo $turno->getNome(); ?>",
        <?php $i++; ?>
      }
      <?php if($total != $i ) : ?>
        ,
      <?php endif; :?>
    <?php endforeach; ?>
  ]
}
