<?php
  $total = count($turnos);
  $i = 0;
?>

{
  "turnos" : [
    <?php foreach($cursos as $curso): ?>
      {
        "<?php echo "id"; ?>":<?php echo $curso->getId(); ?>",
        "<?php echo "nome"; ?>":<?php echo $curso->getNome(); ?>",
        <?php $i++; ?>
      }
      <?php if($total != $i ) : ?>
        ,
      <?php endif; :?>
    <?php endforeach; ?>
  ]
}
