function mostrarTurnos() {
  var turnos = document.getElementsByClassName('turnos')[0];
  if (this.turnoExpandido) {
    turnos.style.display='none';
    this.turnoExpandido = false;
  } else {
    turnos.style.display='block';
    this.turnoExpandido = true;
  }
}

function mostrarCursos() {
  var turnos = document.getElementsByClassName('cursos')[0];
  if (this.cursoExpandido) {
    turnos.style.display='none';
    this.cursoExpandido = false;
  } else {
    turnos.style.display='block';
    this.cursoExpandido = true;
  }
}

function mostrarNiveis() {
  var turnos = document.getElementsByClassName('niveis')[0];
  if (this.nivelExpandido) {
    turnos.style.display='none';
    this.nivelExpandido = false;
  } else {
    turnos.style.display='block';
    this.nivelExpandido = true;
  }
}

function mostrarDisciplinas() {
  var turnos = document.getElementsByClassName('disciplinas')[0];
  if (this.disciplinaExpandida) {
    turnos.style.display='none';
    this.disciplinaExpandida = false;
  } else {
    turnos.style.display='block';
    this.disciplinaExpandida = true;
  }
}

function adicionar() {
  var adicionar = document.getElementById('nova-categoria');
  var menu = document.getElementById('menu-principal');
  menu.style.display = 'none';
  adicionar.style.display = 'block';
}

function menu() {
  var adicionar = document.getElementById('nova-categoria');
  var menu = document.getElementById('menu-principal');
  menu.style.display = 'block';
  adicionar.style.display = 'none';
}

function catForm() {
  var adicionar = document.getElementById('cat');
  adicionar.style.display = 'block';
}

function mostrarTestes() {
  var tt = document.getElementById('tt');
  if (!this.z) {
    tt.style.display = 'block';
    this.z = true;
  } else {
    tt.style.display = 'none';
    this.z = false;
  }
}

function mostrarTestes2() {
  var tt = document.getElementById('tt2');
  if (!this.z2) {
    tt.style.display = 'block';
    this.z2 = true;
  } else {
    tt.style.display = 'none';
    this.z2 = false;
  }
}

function mostrarAvaliacoes(source) {
  var detalhes = source.getElementsByClassName('detalhe-de-avaliacoes')[0];
  alert(source);
}
