<?php
  require_once __DIR__ . '/configuracoes/exportar.php';
  require_once __DIR__ . '/http/exportar.php';

  $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

  if (preg_match(IMG, $url, $array)) {

    $path = __DIR__ . "/public/img/{$array['ficheiro']}.{$array['ext']}";
    if (file_exists($path)) {
      $size = getimagesize($path);

      $mime = $size['mime'];
      $filesize = filesize($path);
      if ($mime === 'image/png') {
        $image = imagecreatefrompng($path);
      } else if ($mime === 'image/jpeg') {
        $image = imagecreatefromjpeg($path);
      } else {
        die('formato nao suportada');
      }

      if (isset($_GET['w']) && isset($_GET['h'])) {
        $w = $_GET['w'];
        $h = $_GET['h'];
        $image = imagescale($image, $w, $h, IMG_BICUBIC_FIXED);
      }

      header("Content-Type: $mime");
      // header("Content-Length: $filesize");
      imagepng($image);
      imagedestroy($image);
    }
  } else if (preg_match(JS, $url, $array)) {
    header('Content-Type: text/javascript');
    $path = __DIR__ . "/public/js/{$array['ficheiro']}.js";
    if (file_exists($path)) {
      require_once $path;
    }
  } else if (preg_match(CSS, $url, $array)) {
    header('Content-Type: text/css');
    $path = __DIR__ . "/public/css/{$array['ficheiro']}.css";
    if (file_exists($path)) {
      require_once $path;
    }
  } else if (preg_match(ROOT, $url, $array)) {
    SessaoControlador::login();
  } elseif (preg_match(LOGIN, $url, $array)) {
    SessaoControlador::login();
  } elseif (preg_match(LOGOUT, $url, $array)) {
    SessaoControlador::logout();
  } elseif (preg_match(HOME, $url, $array)) {
    SessaoControlador::home();
  } elseif (preg_match(PLANO_ANALITICO, $url, $array)) {
    echo '/home/s/plano';
  } elseif (preg_match(CALENDARIO, $url, $array)) {
    echo '/home/s/calendario';
  } elseif (preg_match(PAUTA, $url, $array)) {
    echo '/home/s/pauta';
  } else {
    echo 'erro 404';
  }

?>
